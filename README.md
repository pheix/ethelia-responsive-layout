# Responsive layout for Ethelia project

Responsive layout implemented with Slides Framework **v4.1** for [Ethelia project](https://gitlab.com/pheix/raku-ethelia).

## Credits

- Apotheosis webdev bureau — [on Twitter](https://twitter.com/ApopheozRu), [on Web](https://apopheoz.ru)

- [Slides Framework](https://slides.gitbook.io/manual/)

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
